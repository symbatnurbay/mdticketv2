import mongoose from 'mongoose'

const historyClientSchema = mongoose.Schema({
  user: { type: mongoose.Schema.ObjectId, ref: 'User' },
  ticket: { type: mongoose.Schema.ObjectId, ref: 'Ticket' },
  text: Array
},
{
  timestamps: true
})

module.exports = mongoose.model('HistoryClient', historyClientSchema)
