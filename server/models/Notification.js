import mongoose from 'mongoose'

const notificationSchema = mongoose.Schema({
  user: [{ type: mongoose.Schema.ObjectId, ref: 'User' }],
  type: String,
  ticket: { type: mongoose.Schema.ObjectId, ref: 'Ticket' },
  text: Array,
  read: { type: Boolean, default: false }
},
{
  timestamps: true
})

module.exports = mongoose.model('Notification', notificationSchema)
