import mongoose from 'mongoose'

const historyStaffSchema = mongoose.Schema({
  user: [{ type: mongoose.Schema.ObjectId, ref: 'User' }],
  type: String,
  ticket: { type: mongoose.Schema.ObjectId, ref: 'Ticket' },
  // user2: { type: mongoose.Schema.ObjectId, ref: 'User' },
  text: Array
},
{
  timestamps: true
})

module.exports = mongoose.model('HistoryStaff', historyStaffSchema)
