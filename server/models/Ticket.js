import mongoose from 'mongoose'

const ticketSchema = mongoose.Schema({
  ticketTitle: String,
  number: Number,
  type: { type: String, default: 'open' },
  favorite: { type: Boolean, default: false },
  client: { type: mongoose.Schema.ObjectId, ref: 'User' },
  user: { type: mongoose.Schema.ObjectId, ref: 'User' },
  texts: [{
    type: { type: String }, // ticket, answer, note
    text: String,
    date: Date,
    files: Array,
    refUsers: [{ type: mongoose.Schema.ObjectId, ref: 'User' }],
    user: { type: mongoose.Schema.ObjectId, ref: 'User' },
    messageId: String
  }],
  status: { type: String, default: 'active' }, // active, archived
  deadline: Date,
  ticketFrom: String,
  threadId: String,
  read: { type: Boolean, default: false }
},
{
  timestamps: true
})

module.exports = mongoose.model('Ticket', ticketSchema)
