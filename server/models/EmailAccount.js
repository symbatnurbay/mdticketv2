import mongoose from 'mongoose'

const emailAccountSchema = mongoose.Schema({
  token: Object
},
{
  timestamps: true
})

module.exports = mongoose.model('EmailAccount', emailAccountSchema)
