import mongoose from 'mongoose'

const clientSchema = mongoose.Schema({
  user: {
    type: mongoose.Schema.ObjectId,
    ref: 'User'
  },
  tickets: Array
},
{
  timestamps: true
})

module.exports = mongoose.model('Client', clientSchema)
