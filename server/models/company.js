import mongoose from 'mongoose'
mongoose.models = {};
mongoose.modelSchemas = {};

const companySchema = mongoose.Schema({
  email: String,
  name: String,
  city: String,
  country: String,
  description: String,
  emailModule: {}
},
{
  timestamps: true
})

module.exports = mongoose.model('Company', companySchema)
