import mongoose from 'mongoose'

const historyProfileSchema = mongoose.Schema({
  user: { type: mongoose.Schema.ObjectId, ref: 'User' },
  userTo: { type: mongoose.Schema.ObjectId, ref: 'User' },
  text: Array,
  text2: Array,
  oldData: String,
  newData: String
},
{
  timestamps: true
})

module.exports = mongoose.model('HistoryProfile', historyProfileSchema)
