import mongoose from 'mongoose'
import crypto from 'crypto'
import jwt from 'jsonwebtoken'

/*
 * name: имя фамилия
 * status: active or not active
 * type: admin or employee, or client
 */

const userSchema = mongoose.Schema({
  name: String,
  email: {
    type: String
  },
  username: {
    type: String,
    unique: true
  },
  company: {
    type: mongoose.Schema.ObjectId,
    ref: 'Company'
  },
  status: {
    type: String,
    default: 'active'
  },
  avatar: {
    type: String,
    default: '/images/photo-placeholder.png'
  },
  state: String,
  city: String,
  position: String,
  hash: String,
  salt: String,
  type: String,
  emailAccount: { type: mongoose.Schema.ObjectId, ref: 'EmailAccount' }
},
{
  timestamps: true
})

// Функция для создания пароля
userSchema.methods.setPassword = function (password) {
  this.salt = crypto.randomBytes(16).toString('hex')
  this.hash = crypto.pbkdf2Sync(password, this.salt, 1000, 64, 'sha512').toString('hex')
}

// Функция для проверки валидности пароля
userSchema.methods.validPassword = function (password) {
  var hash = crypto.pbkdf2Sync(password, this.salt, 1000, 64, 'sha512').toString('hex')
  return this.hash === hash
}

// генерирует JsonWebToken, ставит срок истечения на 7 дней
userSchema.methods.generateJwt = function () {
  var expiry = new Date()
  expiry.setDate(expiry.getDate() + 7)

  return jwt.sign({
    _id: this._id,
    email: this.email,
    name: this.username,
    type: this.type,
    company: this.company,
    exp: parseInt(expiry.getTime() / 1000)
  }, 'MDTICKET')
}

userSchema.set('toJSON', {
  transform: (doc, ret, options) => {
    var retJson = {
      _id: ret._id,
      email: ret.email,
      username: ret.username,
      status: ret.status,
      name: ret.name,
      state: ret.state,
      city: ret.city,
      position: ret.position,
      company: ret.company,
      avatar: ret.avatar,
      type: ret.type
    }
    return retJson
  }
})

module.exports = mongoose.model('User', userSchema)
