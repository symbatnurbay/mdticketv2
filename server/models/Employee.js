import mongoose from 'mongoose'

const employeeSchema = mongoose.Schema({
  user: {
    type: mongoose.Schema.ObjectId,
    ref: 'User'
  },
  answers: Array,
  position: String
},
{
  timestamps: true
})

module.exports = mongoose.model('Employee', employeeSchema)
