import passport from 'passport'
import User from '../models/user'
import Company from '../models/Company'
import emailExistence from 'email-existence'

const signup = (req, res, next) => {
  emailExistence.check(req.body.email, (err, ans) => {
    if (ans) {
      const company = new Company()
      company.name = req.body.company
      company.email = req.body.email
      company.save((err, c) => {
        if (err) {
          return next(err)
        }
        User.findOne({ username: req.body.username }).exec((err, u) => {
          if (err) {
            return next(err)
          }
          if (u) {
            return res.status(403).send({ message: 'Пользователь с таким логином существует', origin: 'username' })
          }

          const user = new User()

          user.username = req.body.username
          user.email = c.email
          user.name = req.body.name
          user.company = c._id
          user.status = 'active'
          user.type = 'admin'
          user.setPassword(req.body.password)

          user.save((err, user) => {
            if (err) {
              if (err.code === 11000) {
                return res.status(418).send({ message: 'Пользователь с такой почтой уже существует', origin: 'email' })
              }
            }
            const token = user.generateJwt()
            res.status(200).send({ token })
          })
        })
      })
    } else {
      return res.status(404).send({ message: 'Введите правильный email', origin: 'email' })
    }
  })
}

const login = (req, res, next) => {
  passport.authenticate('local', (err, user, info) => {
    let token
    if (err) {
      next(err)
    }
    if (!user) {
      return res.status(404).send(info)
    }
    if (user) {
      token = user.generateJwt()
      res.status(200).send({ token })
    }
  })(req, res)
}

module.exports = {
  signup,
  login
}
