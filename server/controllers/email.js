const Imap = require('imap')
const simpleParser = require('mailparser').simpleParser
const ctrlJobProcesses = require('./jobProcesses')
const socketio = require('socket.io-emitter')({ host: '127.0.0.1', port: 6379 })
const ctrlClients = require('./clients')
const utilPersonal = require('../utils/personal')

import fs from 'fs'
import Ticket from '../models/Ticket'
import User from '../models/user'
import Company from '../models/company'
import moment from 'moment'
import inbox from 'inbox'
import emailExistence from 'email-existence'
import planer from 'planer'

const saveCredentials = (dataUser, response) => {
  var tempObj = {
    email: dataUser.email,
    password: utilPersonal.encrypt(dataUser.password), // нужно поменять
    imap: dataUser.imap,
    smtp: dataUser.smtp
  }
  Company.findOneAndUpdate({ _id: dataUser.company }, { $set: { emailModule: tempObj }}, { new: true }, (err, data) => {
    if (err) console.log(err)

    response(data)
  })
}

const getEmails = (req, res) => {
  const imap = new Imap({
    user: req.body.email,
    password: req.body.password,
    host: req.body.imap,
    port: 993,
    tls: true
  })

  function openInbox (cb) {
    imap.openBox('INBOX', false, cb)
  }

  imap.once('ready', () => {
    openInbox((err, box) => {
      if (err) console.log(err, 28)

      var temp = req.body
      temp.company = req.payload.company
      saveCredentials(temp, (data) => {
        imap.search(['UNSEEN'], (err, results) => {
          if (err) console.log(err, 31)

          if (results && results.length > 0) {
            imap.setFlags(results, ['\\Seen'], (err) => {
              if (err) console.log(err, 34)

              console.log('message read')
            })
            var f = imap.fetch(results, { bodies: ['', ''], struct: true })
            f.on('message', (msg, seqno) => {
              msg.on('body', (stream, info) => {
                simpleParser(stream).then(mail => {
                  emailExistence.check(mail.from.value[0].address, (err, ans) => {
                    if (ans) {
                      User.findOne({ email: mail.from.value[0].address, type: 'client' }, (err, client) => {
                        if (err) console.log(err, 43)

                        if (client) {
                          addTicket(mail, client)
                        } else {
                          ctrlClients.createClientEmail(mail.from.value[0].address, mail.from.value[0].name, data, (err, client) => {
                            if (err) console.log(err, 49)

                            addTicket(mail, client)
                          })
                        }
                      })
                    }
                  })
                }).catch(err => {
                  console.log(err, 56)
                })
              })
            })
            f.once('error', (err) => {
              console.log('Fetch error: ' + err, 61)
            })
            f.once('end', () => {
              res.status(200).send('email parsed')
            })
          } else {
            res.status(200).send('empty')
          }
        })
      })
    })
  })

  imap.once('error', (err) => {
    console.log(err, 72)
    res.status(404).send('not found')
  })

  imap.connect()
}

const addTicket = (mail, client) => {
  Ticket.count({ client: client._id }, (err, tickets) => {
    if (err) console.log(err, 80)

    Ticket.findOne({ texts: { $elemMatch: { messageId: mail.messageId }}}, (err, ticket) => {
      if (err) console.log(err, 83)

      if (!ticket) {
        if (mail.inReplyTo) {
          replyTicket(mail)
        } else {
          createTicket(mail, tickets + 1, client)
        }
      }
    })
  })
}

const createTicket = (mail, number, client) => {
  var tempObj = {}
  tempObj.ticketTitle = mail.subject
  tempObj.number = number
  tempObj.client = client._id
  tempObj.threadId = mail.messageId
  tempObj.deadline = moment(mail.date).add(3, 'hours')
  tempObj.ticketFrom = 'email'
  tempObj.texts = [{
    type: 'ticket',
    text: mail.text ? planer.extractFromPlain(mail.text) : mail.text,
    date: mail.date,
    files: [],
    messageId: mail.messageId
  }]
  if (mail.attachments && mail.attachments.length > 0) {
    var x = ''
    var tempFilename = ''
    mail.attachments.forEach(item => {
      x = item.content.toString('base64')
      tempFilename = item.filename.split('.')
      fs.writeFile('./static/files/' + tempFilename[0] + Date.now() + '.' + tempFilename[1], x, { encoding: 'base64' }, (err) => {
        if (err) throw new Error(err, 118)
      })
      tempObj.texts[0].files.push({ path: '/files/' + tempFilename[0] + Date.now() + '.' + tempFilename[1], name: item.filename })
    })
  }
  const ticket = new Ticket(tempObj)
  ticket.save(err => {
    if (err) console.log(err, 125)

    Ticket.populate(ticket, { path: 'client user texts.user texts.refUsers' }, (err) => {
      if (err) console.log(err, 128)

      ctrlJobProcesses.createDeadline(ticket._id, tempObj.deadline)
      socketio.emit('ticketFromEmailAdded', ticket)
    })
  })
}

const replyTicket = (mail) => {
  var tempObj = {}
  Ticket.findOne({ texts: { $elemMatch: { messageId: mail.inReplyTo }}}, (err, ticket) => {
    if (err) console.log(err, 140)

    if (ticket) {
      tempObj = {
        type: 'ticket',
        text: mail.text ? planer.extractFromPlain(mail.text) : mail.text,
        date: mail.date,
        files: [],
        messageId: mail.messageId
      }
      if (mail.attachments && mail.attachments.length > 0) {
        var x = ''
        var tempFilename = ''
        mail.attachments.forEach(item => {
          x = item.content.toString('base64')
          tempFilename = item.filename.split('.')
          fs.writeFile('./static/files/' + tempFilename[0] + Date.now() + '.' + tempFilename[1], x, { encoding: 'base64' }, (err) => {
            if (err) console.log(err, 156)
          })
          tempObj.files.push({ path: '/files/' + tempFilename[0] + Date.now() + '.' + tempFilename[1], name: item.filename })
        })
      }
      ticket.texts.push(tempObj)
      ticket.read = false
      ticket.save(err => {
        if (err) console.log(err, 163)

        Ticket.populate(ticket, { path: 'client user texts.user texts.refUsers' }, (err) => {
          if (err) console.log(err, 166)

          socketio.emit('ticketFromEmailChanged', ticket)
        })
      })
    }
  })
}

const inspectNewMessages = () => {
  Company.find({ emailModule: { $exists: true }}, (err, companies) => {
    if (err) console.log(err)

    if (companies) {
      companies.forEach((company) => {
        var client = inbox.createConnection(false, company.emailModule.imap, {
          secureConnection: true,
          auth: {
            user: company.emailModule.email,
            pass: utilPersonal.decrypt(company.emailModule.password)
          }
        })

        client.connect()

        client.on('connect', () => {
          client.openMailbox('INBOX', false, (error, info) => {
            if (error) throw error

            client.on('new', (message) => {
              client.addFlags(message.UID, ['\\Seen'], (error, flags) => {
                if (error) throw error

                var messageStream = client.createMessageStream(message.UID)
                simpleParser(messageStream).then(mail => {
                  emailExistence.check(mail.from.value[0].address, (err, ans) => {
                    if (ans) {
                      User.findOne({ email: mail.from.value[0].address, type: 'client' }, (err, client) => {
                        if (err) console.log(err, 243)

                        if (client) {
                          addTicket(mail, client)
                        } else {
                          ctrlClients.createClientEmail(mail.from.value[0].address, mail.from.value[0].name, company, (err, client) => {
                            if (err) console.log(err, 249)

                            addTicket(mail, client)
                          })
                        }
                      })
                    }
                  })
                })
              })
            })
          })
        })
      })
    }
  })
}

module.exports = {
  getEmails,
  saveCredentials,
  inspectNewMessages
}
