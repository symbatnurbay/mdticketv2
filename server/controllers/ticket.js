import Ticket from '../models/Ticket'
import Client from '../models/Client'
import User from '../models/user'
import Company from '../models/company'
import moment from 'moment'
const ctrlJobProcesses = require('./jobProcesses')
const utilMail = require('../utils/mail')
const jobs = require('../queue/').jobs

const addTicket = (req, res) => {
  var client = req.payload
  var date = moment().add(3, 'hours')
  Ticket.find().populate('client').exec((err, tickets) => {
    if (err) throw new Error(err)
    var i = 1
    if (tickets && tickets.length > 0) {
      tickets.forEach((item) => {
        if (item.client.company.toString() === client.company) {
          i++
        }
      })
    }
    var ticket = new Ticket()
    ticket.ticketTitle = req.body.title
    ticket.number = i
    ticket.client = client._id
    ticket.user = null
    ticket.deadline = date
    ticket.ticketFrom = 'internal'
    ticket.texts = [
      {
        text: req.body.text,
        type: 'ticket',
        date: Date.now(),
        files: req.files.map(file => {
          return {
            path: `/files/${file.filename}`,
            name: file.originalname
          }
        })
      }
    ]
    ticket.save((err) => {
      if (err) throw new Error(err)

      Ticket.populate(ticket, { path: 'client' }, (err) => {
        if (err) throw new Error(err)

        ctrlJobProcesses.createDeadline(ticket._id, date)
        res.status(200).send({ ticket })
      })
    })
  })
}

const getTickets = (req, res) => {
  var user = req.payload
  if (user.type === 'client') {
    Ticket.find({ client: user._id, status: 'active' }).populate('client user texts.user texts.refUsers').exec((err, tickets) => {
      if (err) throw new Error(err)

      tickets.sort((a, b) => {
        return new Date(a.deadline) - new Date(b.deadline)
      })
      res.status(200).send(tickets)
    })
  } else {
    User.find({ company: user.company, type: 'client' }, (err, users) => {
      if (err) throw new Error(err)

      var temp = []
      users.forEach((item) => {
        temp.push(item._id)
      })
      Ticket.find({ client: { $in: temp }, status: 'active' }).populate('client user texts.user texts.refUsers').exec((err, tickets) => {
        if (err) throw new Error(err)

        // tickets.sort((a, b) => {
        //   return new Date(a.deadline) - new Date(b.deadline)
        // })
        res.status(200).send(tickets)
      })
    })
  }
}

const addAnswer = (req, res) => {
  var ticketId = req.params.id
  var answer = {
    type: 'ticket',
    text: req.body.answerType === 'note' ? req.body.note : req.body.text,
    date: Date.now(),
    files: req.files ? req.files.map(file => {
      return {
        path: `/files/${file.filename}`,
        name: file.originalname
      }
    }) : '',
    refUsers: req.body.refUsers ? req.body.refUsers : []
  }
  let ticketUser = JSON.parse(req.body.user)
  ticketUser = !JSON.parse(req.body.user) || JSON.parse(req.body.user)._id === 'user1' || !JSON.parse(req.body.user)._id ? null : JSON.parse(req.body.user)._id
  if (req.payload.type !== 'client') {
    answer.type = req.body.answerType
    answer.user = req.payload._id
  }
  const type = req.body.oldType === 'closed' && ((req.body.type === 'closed' && req.body.answerType !== 'note') || answer.type === 'ticket') ? 'open' : req.body.type
  Ticket.findOneAndUpdate({ _id: ticketId },
    { $set: { user: ticketUser, type }, $push: { 'texts': answer }},
    { new: true }).populate('client user texts.user texts.refUsers').exec((err, ticket) => {
    if (err) throw new Error(err)

    if (ticket.ticketFrom && ticket.ticketFrom === 'email' && req.payload.type !== 'client') {
      Company.findById(req.payload.company, (err, company) => {
        if (err) throw new Error(err)

        var tempAtt = []
        var tempRef = []

        if (answer.files && answer.files.length > 0) {
          answer.files.forEach(item => {
            tempAtt.push({
              filename: item.name,
              path: item.path
            })
          })
        }
        ticket.texts.forEach(item => {
          if (item.messageId) {
            tempRef.push(item.messageId)
          }
        })
        var mail = {
          to: ticket.client.email,
          subject: ticket.ticketTitle,
          text: answer.text,
          html: answer.text,
          inReplyTo: tempRef[tempRef.length - 1],
          attachments: tempAtt,
          references: tempRef
        }
        utilMail.sendReplyMail(company, mail, (err, ans) => {
          if (err) throw new Error(err)

          ticket.texts[ticket.texts.length - 1].messageId = ans
          ticket.save(err => {
            if (err) throw new Error(err)

            Ticket.populate(ticket, { path: 'client user texts.user texts.refUsers' }, (err) => {
              if (err) throw new Error(err)

              res.status(200).send(ticket)
            })
          })
        })
      })
    } else {
      res.status(200).send(ticket)
    }
  })
}

const changeFavAll = (req, res) => {
  var ticketIds = []
  req.body.tickets.forEach((item) => {
    ticketIds.push(item._id)
  })
  Ticket.update({ _id: { $in: ticketIds }}, { $set: { favorite: !req.body.value }}, { multi: true }, (err) => {
    if (err) throw new Error(err)

    res.status(200).end()
  })
}

const changeFavOne = (req, res) => {
  Ticket.findOneAndUpdate({ _id: req.body.id }, { $set: { favorite: !req.body.value }}, (err) => {
    if (err) throw new Error(err)

    res.status(200).end()
  })
}

const deleteOneTicket = (req, res) => {
  Ticket.findOneAndUpdate({ _id: req.params.id }, { $set: { status: 'archived' }}, (err) => {
    if (err) throw new Error(err)

    res.status(200).end()
  })
}

const deleteTickets = (req, res) => {
  var ticketIds = req.body.tickets
  if (ticketIds[0] === 'all') {
    ticketIds.splice(0, 1)
  }
  Ticket.update({ _id: { $in: ticketIds }}, { $set: { status: 'archived' }}, { multi: true }, (err) => {
    if (err) throw new Error(err)

    res.status(200).end()
  })
}

const assignUserChecked = (req, res) => {
  var ticketIds = req.body.tickets
  if (ticketIds[0] === 'all') {
    ticketIds.splice(0, 1)
  }
  var user = req.body.user === 'user1' || !req.body.user ? null : req.body.user
  Ticket.update({ _id: { $in: ticketIds }}, { $set: { user: user }}, { multi: true }, (err) => {
    if (err) throw new Error(err)

    res.status(200).end()
  })
}

const assignUser = (req, res) => {
  var user = req.body.user === 'user1' ? null : req.body.user
  Ticket.findOneAndUpdate({ _id: req.params.id }, { $set: { user: user }}, (err) => {
    if (err) throw new Error(err)

    res.status(200).end()
  })
}

const changeType = (req, res) => {
  Ticket.findOneAndUpdate({ _id: req.params.id }, { $set: { type: req.body.type }}, (err) => {
    if (err) throw new Error(err)

    res.status(200).end()
  })
}

const changeTypeChecked = (req, res) => {
  var ticketIds = req.body.tickets
  if (ticketIds[0] === 'all') {
    ticketIds.splice(0, 1)
  }
  Ticket.update({ _id: { $in: ticketIds }}, { $set: { type: req.body.type }}, { multi: true }, (err) => {
    if (err) throw new Error(err)

    res.status(200).end()
  })
}

const snoozeTicket = (req, res) => {
  Ticket.findOneAndUpdate({ _id: req.params.id }, { $set: { deadline: req.body.value }}, (err, ticket) => {
    if (err) throw new Error(err)

    ctrlJobProcesses.changeDeadline(ticket._id)
    ctrlJobProcesses.createDeadline(ticket._id, req.body.value)
    res.status(200).send({ data: req.body.value })
  })
}

const changeRead = (req, res) => {
  console.log('yo')
  Ticket.findOneAndUpdate({ _id: req.params.id }, { $set: { read: true }}, (err) => {
    if (err) throw new Error(err)

    res.status(200).end()
  })
}

module.exports = {
  addTicket,
  getTickets,
  addAnswer,
  changeFavAll,
  changeFavOne,
  deleteOneTicket,
  deleteTickets,
  assignUserChecked,
  assignUser,
  changeType,
  changeTypeChecked,
  snoozeTicket,
  changeRead
}
