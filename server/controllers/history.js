import User from '../models/user'
import Ticket from '../models/Ticket'
import HistoryStaff from '../models/HistoryStaff'
import HistoryClient from '../models/HistoryClient'
import HistoryProfile from '../models/HistoryProfile'

const getHistory = (req, res) => {
  var user = req.payload
  if (user.type === 'client') {
    Ticket.find({ client: user._id }, (err, tickets) => {
      if (err) throw new Error(err)

      var ticketIds = []
      tickets.forEach((item) => {
        ticketIds.push(item._id)
      })
      HistoryClient.find({ ticket: { $in: ticketIds }}).populate('user ticket').exec((err, history) => {
        if (err) throw new Error(err)

        history.sort((a, b) => {
          return new Date(b.createdAt) - new Date(a.createdAt)
        })
        res.status(200).send(history)
      })
    })
  } else {
    User.find({ company: user.company }, (err, users) => { // type: { $ne: 'client' }
      if (err) throw new Error(err)

      var userIds = []
      users.forEach((item) => {
        userIds.push(item._id)
      })
      HistoryStaff.find({ user: { $in: userIds }}).populate('user ticket').exec((err, history) => {
        if (err) throw new Error(err)

        history.sort((a, b) => {
          return new Date(b.createdAt) - new Date(a.createdAt)
        })

        res.status(200).send(history)
      })
    })
  }
}

const getHistoryProfile = (req, res) => {
  var user = req.params.id
  HistoryProfile.find({ userTo: user }).populate('user userTo').exec((err, history) => {
    if (err) throw new Error(err)

    history.sort((a, b) => {
      return new Date(b.createdAt) - new Date(a.createdAt)
    })

    res.status(200).send(history)
  })
}

module.exports = {
  getHistory,
  getHistoryProfile
}
