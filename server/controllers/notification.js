import Notification from '../models/Notification'

const getNotifications = (req, res) => {
  var user = req.payload
  Notification.find().populate('user ticket').exec((err, n) => {
    if (err) throw new Error(err)

    var notifications = []
    n.forEach((item) => {
      if (item.user[0]._id.toString() === user._id.toString()) {
        notifications.push(item)
      }
    })
    notifications.sort((a, b) => {
      return new Date(b.createdAt) - new Date(a.createdAt)
    })
    res.status(200).send(notifications)
  })
}

const changeUnread = (req, res) => {
  var ids = []
  req.body.forEach((item) => {
    ids.push(item._id)
  })
  Notification.update({ _id: { $in: ids }}, { $set: { read: true }}, { multi: true }, (err) => {
    if (err) throw new Error(err)

    res.status(200).send(ids)
  })
}

module.exports = {
  getNotifications,
  changeUnread
}
