import User from '../models/user'
import Company from '../models/Company'
import Ticket from '../models/Ticket'
import passGen from 'password-generator'
import mailsender from '../utils/mail'
import Mailgen from 'mailgen'
import fs from 'fs'
import path from 'path'
import emailExistence from 'email-existence'

const getUsers = (req, res, next) => {
  const companyId = req.payload.company
  User.find({ company: companyId, type: { $ne: 'client' }}, (err, users) => {
    if (err) return next(err)
    res.status(200).send(users)
  })
}

const getUserById = (req, res, next) => {
  User.findById(req.params.id).populate('company').exec((err, user) => {
    if (err) return next(err)
    res.status(200).send(user)
  })
}

const updateUser = (req, res, next) => {
  User.findById(req.params.id).exec((err, user) => {
    if (err) return next(err)
    user.name = req.body.name || user.name
    user.state = req.body.state || user.state
    user.city = req.body.city || user.city
    user.email = req.body.email || user.email
    user.position = req.body.position || user.position
    user.save((err, user) => {
      if (err) return next(err)
      User.findOne({ _id: user._id }).populate('company').exec((err, user) => {
        if (err) return next(err)
        res.status(200).send({ user, message: 'Данные успешно обновлены' })
      })
    })
  })
}

const add = (req, res, next) => {
  User.findOne({ username: req.body.username }, (err, user) => {
    if (err) return next(err)
    if (user) {
      return res.status(400).send({ message: 'Пользователь с такими данными существует', origin: 'username' })
    }
    if (!user) {
      emailExistence.check(req.body.email, (err, ans) => {
        if (ans) {
          const newUser = new User({
            username: req.body.username,
            type: 'employee',
            email: req.body.email,
            name: req.body.name,
            company: req.payload.company,
            position: req.body.position
          })
          const password = passGen()
          newUser.setPassword(password)
          newUser.save((err, user) => {
            if (err) return next(err)

            var mailGenerator = new Mailgen({
              theme: 'default',
              product: {
                name: 'MD Ticket',
                link: 'http://146.185.131.163:8000/#/'
              }
            })

            var email = {
              body: {
                greeting: 'Здравствуйте,',
                name: req.body.name,
                intro: ['Спасибо, что воспользовались нашей системой.', `Ваш ID: ${user.username}<br>Ваш пароль: ${password}`]
              }
            }

            var emailBody = mailGenerator.generate(email)
            mailsender.sendMail(user.email, 'Регистрация в MD ticket', '', emailBody, (err, message) => {
              if (err) return next(err)
            })
            res.status(200).send({ message: 'Сотрудник успешно создан', user })
          })
        } else {
          res.status(404).send({ message: 'email not found' })
        }
      })
    }
  })
}

const getMe = (req, res, next) => {
  User.findOne({ _id: req.payload._id }).populate('company').exec((err, user) => {
    if (err) return next(err)

    res.status(200).send({ user })
  })
}

const updateMe = (req, res, next) => {
  User.findOne({ _id: req.payload._id }).populate('company').exec((err, user) => {
    if (err) return next(err)
    const company = req.body.company || user.company.name
    user.name = req.body.name || user.name
    user.username = req.body.username || user.username
    user.email = req.body.email || user.email
    user.position = req.body.position || user.position
    if (req.body.password && req.body.rePassword) {
      if (req.body.password !== req.body.rePassword) {
        return res.status(400).send({ message: 'Пароли не совпадают', origin: 'rePassword' })
      }
      user.setPassword(req.body.password)
    }
    user.save((err, user) => {
      if (err) return next(err)

      if (req.payload.type === 'admin') {
        Company.findOneAndUpdate({ _id: user.company }, { $set: { name: company }}, (err, company) => {
          if (err) return next(err)
          User.findOne({ _id: req.payload._id }).populate('company').exec((err, user) => {
            if (err) return next(err)
            res.status(200).send({ user, message: 'Данные успешно обновлены' })
          })
        })
      } else {
        User.findOne({ _id: req.payload._id }).populate('company').exec((err, user) => {
          if (err) return next(err)
          res.status(200).send({ user, message: 'Данные успешно обновлены' })
        })
      }
    })
  })
}

const changeAvatar = (req, res, next) => {
  User.findById(req.params.id).exec((err, user) => {
    if (err) return next(err)
    if (user.avatar) {
      if (!user.avatar === '/images/photo-placeholder.png') {
        fs.unlink(path.join('./static', user.avatar), err => {
          if (err) return next(err)
          user.avatar = `/images/avatars/${req.file.filename}`
          user.save((err, user) => {
            if (err) return next(err)

            User.populate(user, { path: 'company' }, (err) => {
              if (err) throw new Error(err)
              res.status(200).send({ user, message: 'Аватар успешно удален' })
            })
          })
        })
      } else {
        user.avatar = `/images/avatars/${req.file.filename}`
        user.save((err, user) => {
          if (err) return next(err)

          User.populate(user, { path: 'company' }, (err) => {
            if (err) throw new Error(err)
            res.status(200).send({ user, message: 'Аватар успешно удален' })
          })
        })
      }
    } else {
      user.avatar = `/images/avatars/${req.file.filename}`
      user.save((err, user) => {
        if (err) return next(err)

        User.populate(user, { path: 'company' }, (err) => {
          if (err) throw new Error(err)
          res.status(200).send({ user, message: 'Аватар успешно удален' })
        })
      })
    }
  })
}

const deleteImage = (req, res, next) => {
  User.findById(req.params.id).exec((err, user) => {
    if (err) return next(err)
    fs.unlink(path.join('./static', user.avatar), err => {
      if (err) return next(err)
      user.avatar = '/images/photo-placeholder.png'
      user.save((err, user) => {
        if (err) return next(err)

        User.populate(user, { path: 'company' }, (err) => {
          if (err) throw new Error(err)
          res.status(200).send({ user, message: 'Аватар успешно удален' })
        })
      })
    })
  })
}

const deleteUser = (req, res) => {
  User.deleteMany({ _id: { $in: req.body.users }}, (err) => {
    if (err) throw new Error(err)

    if (req.body.type === 'staff') {
      Ticket.update({ user: { $in: req.body.users }}, { $set: { user: null }}, { multi: true }, (err) => {
        if (err) throw new Error(err)

        res.status(200).end()
      })
    } else {
      Ticket.update({ user: { $in: req.body.users }}, { $set: { status: 'archived' }}, { multi: true }, (err) => {
        if (err) throw new Error(err)

        res.status(200).end()
      })
    }
  })
}

module.exports = {
  getUsers,
  add,
  getMe,
  updateMe,
  deleteUser,
  changeAvatar,
  getUserById,
  updateUser,
  deleteImage
}
