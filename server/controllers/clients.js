import User from '../models/user'
import passGen from 'password-generator'
import mailsender from '../utils/mail'
import Mailgen from 'mailgen'
import emailExistence from 'email-existence'

const add = (req, res, next) => {
  User.findOne({ username: req.body.username }, (err, user) => {
    if (err) return next(err)
    if (user) {
      return res.status(400).send({ message: 'Пользователь с такими данными существует', origin: 'username' })
    }
    if (!user) {
      emailExistence.check(req.body.email, (err, ans) => {
        if (ans) {
          const newUser = new User({
            username: req.body.username,
            type: 'client',
            email: req.body.email,
            name: req.body.name,
            company: req.payload.company
          })
          const password = passGen()
          newUser.setPassword(password)
          newUser.save((err, user) => {
            if (err) return next(err)

            var mailGenerator = new Mailgen({
              theme: 'default',
              product: {
                name: 'MD Ticket',
                link: 'http://146.185.131.163:8000/#/'
              }
            })

            var email = {
              body: {
                greeting: 'Здравствуйте,',
                name: req.body.name,
                intro: ['Спасибо, что воспользовались нашей системой.', `Ваш ID: ${user.username}<br>Ваш пароль: ${password}`]
              }
            }

            var emailBody = mailGenerator.generate(email)
            mailsender.sendMail(user.email, 'Регистрация в MD ticket', '', emailBody, (err, message) => {
              if (err) return next(err)
            })
            res.status(200).send({ message: 'Клиент успешно создан', user })
          })
        } else {
          res.status(404).send({ message: 'email not found' })
        }
      })
    }
  })
}

const get = (req, res, next) => {
  User.find({ company: req.payload.company, type: 'client' }).exec((err, clients) => {
    if (err) return next(err)
    res.status(200).send(clients)
  })
}

const createClientEmail = (email, name, data, callback) => {
  const newUser = new User({
    username: Date.now(),
    type: 'client',
    email: email,
    name: name,
    company: data._id
  })
  const password = passGen()
  newUser.setPassword(password)
  newUser.save((err, client) => {
    if (err) return callback(err)

    var mailGenerator = new Mailgen({
      theme: 'default',
      product: {
        name: 'MD Ticket',
        link: 'http://146.185.131.163:8000/#/'
      }
    })

    var emailText = {
      body: {
        greeting: 'Здравствуйте,',
        name: name,
        intro: [`Вы отправляли тикет на электронный адрес ${data.emailModule.email}. Так как эта компания является нашим клиентом, вы были автоматически зарегистрированы в нашей системе.`,
          `Для ознакомления с системой, просим Вас пройти по ссылке <a href="http://mdticket.tk/" target="_blank">MDTicket.tk</a>`, `Ваш ID для входа в систему: ${client.username}<br>Ваш пароль: ${password}`]
      }
    }

    var emailBody = mailGenerator.generate(emailText)
    mailsender.sendMail(client.email, 'Регистрация в MD ticket', '', emailBody, (err, message) => {
      if (err) return callback(err)

      callback(null, client)
    })
  })
}

module.exports = {
  add,
  get,
  createClientEmail
}
