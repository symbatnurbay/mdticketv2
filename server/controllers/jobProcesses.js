import kue from 'kue'
import moment from 'moment'

const jobs = require('../queue/').jobs

const changeDeadline = (ticket) => {
  jobs.delayed(( err, ids ) => { // others are active, complete, failed, delayed
    ids.forEach((id, index) => {
      kue.Job.get(id, (err, job) => {
        if (err) throw new Error(err)

        if ((job.type === 'snooze') && (job.data === ticket)) {
          kue.Job.remove(id)
        }
      })
    })
  })
}

const createDeadline = (ticket, date) => {
  var timeTodelete = new Date()
  timeTodelete.setTime(new Date(date).getTime() - 1800000)
  var job = jobs.create('snooze', ticket)
    .delay(timeTodelete)
    .priority('high')
    .removeOnComplete(true)
    .save()
  jobs.process('overlord', (job, done) => {
    handle(job.data, done)
  })
}

module.exports = {
 changeDeadline,
 createDeadline
}