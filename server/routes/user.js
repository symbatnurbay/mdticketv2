import express from 'express'
const router = new express.Router()

import multer from 'multer'
import path from 'path'

const storage = multer.diskStorage({
  destination: 'static/images/avatars',
  filename: function (req, file, cb) {
    const name = path.basename(file.originalname, path.extname(file.originalname)) + Date.now() + path.extname(file.originalname)
    cb(null, name)
  }
})

const upload = multer({ storage })

const ctrlUser = require('../controllers/user')

router.get('/', ctrlUser.getUsers)

router.post('/', ctrlUser.add)

router.get('/me', ctrlUser.getMe)

router.put('/me', ctrlUser.updateMe)

router.post('/:id/avatar', upload.single('avatar'), ctrlUser.changeAvatar)

router.get('/:id', ctrlUser.getUserById)

router.put('/:id', ctrlUser.updateUser)

router.delete('/:id/avatar', ctrlUser.deleteImage)

router.post('/delete', ctrlUser.deleteUser)

module.exports = router
