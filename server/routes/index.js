import express from 'express'
const router = express.Router()
import jwt from 'express-jwt'
// import cron from 'cron'
const ctrlEmails = require('../controllers/email')

const auth = jwt({
  secret: 'MDTICKET',
  userProperty: 'payload'
})

router.use('/auth', require('./authentication'))
router.use('/clients', auth, require('./clients'))
router.use('/users', auth, require('./user'))
router.use('/tickets', auth, require('./tickets'))
router.use('/history', auth, require('./history'))
router.use('/notification', auth, require('./notification'))
router.use('/email', auth, require('./email'))

// если запрос под никакой апи не подходит, то оставим это дело ангуляр
router.get('*', (req, res) => {
  res.redirect('/#' + req.originalUrl)
})

// отслеживает письма пользователей
ctrlEmails.inspectNewMessages()

// если возникла какая-то ошибка, то если в обработке запроса мы пишем next(error), все попадает сюда
router.use((err, req, res, next) => {
  console.error(err.stack)

  if (err.name === 'UnauthorizedError') {
    return res.status(401).send('invalid token...')
  }
  res.status(500).send({ message: err.message })
})

module.exports = router
