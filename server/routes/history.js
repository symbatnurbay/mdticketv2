import express from 'express'
const router = new express.Router()

const ctrlHistory = require('../controllers/history')

router.get('/gethistory', ctrlHistory.getHistory)
router.get('/profile/:id', ctrlHistory.getHistoryProfile)

module.exports = router
