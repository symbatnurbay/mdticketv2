import express from 'express'
const router = new express.Router()
import multer from 'multer'
import path from 'path'

const storage = multer.diskStorage({
  destination: 'static/files',
  filename: function (req, file, cb) {
    const name = path.basename(file.originalname, path.extname(file.originalname)) + Date.now() + path.extname(file.originalname)
    cb(null, name)
  }
})

const upload = multer({ storage })

const ctrlTicket = require('../controllers/ticket')

// POST
router.post('/', upload.array('fileToUpload[]'), ctrlTicket.addTicket)
router.post('/addanswer/:id', upload.array('fileToUpload[]'), ctrlTicket.addAnswer)
router.post('/changefavall', ctrlTicket.changeFavAll)
router.post('/changefavone', ctrlTicket.changeFavOne)
router.post('/changeread/:id', ctrlTicket.changeRead)
router.post('/deleteticket/:id', ctrlTicket.deleteOneTicket)
router.post('/deletetickets', ctrlTicket.deleteTickets)
router.post('/assignusers', ctrlTicket.assignUserChecked)
router.post('/assignuser/:id', ctrlTicket.assignUser)
router.post('/changetypes', ctrlTicket.changeTypeChecked)
router.post('/changetype/:id', ctrlTicket.changeType)
router.post('/snoozeticket/:id', ctrlTicket.snoozeTicket)

// GET
router.get('/gettickets', ctrlTicket.getTickets)

module.exports = router
