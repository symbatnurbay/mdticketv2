import express from 'express'
const router = new express.Router()

const ctrlNotification = require('../controllers/notification')

router.get('/', ctrlNotification.getNotifications)
router.post('/unread', ctrlNotification.changeUnread)

module.exports = router
