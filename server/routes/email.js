import express from 'express'

const router = new express.Router()
const ctrlEmails = require('../controllers/email')

router.post('/', ctrlEmails.getEmails)

module.exports = router
