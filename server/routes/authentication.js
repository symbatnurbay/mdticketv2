import express from 'express'
const router = new express.Router()

const ctrlAuth = require('../controllers/authentication')

router.post('/signup', ctrlAuth.signup)

router.post('/login', ctrlAuth.login)

module.exports = router
