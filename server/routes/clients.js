import express from 'express'
const router = new express.Router()

const ctrlClients = require('../controllers/clients')

// POST /api/clients: creating new client
router.post('/', ctrlClients.add)

router.get('/', ctrlClients.get)

module.exports = router
