import express from 'express'
import passport from 'passport'
import morgan from 'morgan'
import bodyParser from 'body-parser'
import socket from 'socket.io'

import mongoose from 'mongoose'
mongoose.connect('mongodb://localhost:27017/ticket')
require('./config/passport')
const app = express()

const s = app.listen(8000, () => {
  console.log('magic is happening on port 8000')
})

const io = socket()
app.io = io
io.attach(s)

const socketEvents = require('./socket')(io)

const queueEvents = require('./queue')
queueEvents.queueEvents()

const redisSocket = require('socket.io-redis')
io.adapter(redisSocket({ host: '127.0.0.1', port: 6379 }))

if (process.env.NODE_ENV !== 'production') {
  const webpack = require('webpack')
  const webpackConfig = require('../webpack.development.js')
  const compiler = webpack(webpackConfig)

  app.use(require('webpack-dev-middleware')(compiler, {
    log: false,
    path: '../client/build/__webpack_hmr',
    heartbeat: 1000
  }))

  app.use(require('webpack-hot-middleware')(compiler))
}

app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())
app.use(morgan('dev'))
app.use(passport.initialize())

import index from './routes/index'
app.use('/api', index)

app.use(express.static('client/build'))
app.use('/images', express.static('static/images'))
app.use('/fonts', express.static('static/fonts'))
app.use('/files', express.static('static/files'))

// Exporting server
export default { app }
