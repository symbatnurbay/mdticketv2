import nodemailer from 'nodemailer'
const utilPersonal = require('./personal')

const sendMail = (to, subject = '', text = '', html = '', done) => {
  const transporter = nodemailer.createTransport({
    host: 'smtp.gmail.com',
    port: 587,
    secure: false,
    auth: {
      user: 'sigismundnoel@gmail.com',
      pass: 'VulferamNoel'
    }
  })

  const mailOptions = {
    from: 'Almat Ybray <almat.ybray@gmail.com>',
    to,
    subject,
    text,
    html
  }

  transporter.sendMail(mailOptions, (err, info) => {
    if (err) console.log(err)
    console.log('Message sent:', info.messageId)
    done(null, 'Письмо отправлено')
  })
}

const sendReplyMail = (company, mail, done) => {
  const transporter = nodemailer.createTransport({
    host: company.emailModule.smtp,
    port: 587,
    secure: false,
    auth: {
      user: company.emailModule.email,
      pass: utilPersonal.decrypt(company.emailModule.password)
    }
  })

  const mailOptions = {
    from: `${company.name} <${company.emailModule.email}>`,
    to: mail.to,
    subject: mail.subject,
    text: mail.text,
    html: mail.html,
    inReplyTo: mail.inReplyTo,
    references: mail.references,
    attachments: mail.attachments
  }

  console.log(mailOptions)

  transporter.sendMail(mailOptions, (err, info) => {
    if (err) return done(err)
    console.log('Message sent:', info.messageId)
    done(null, info.messageId)
  })
}

module.exports = {
  sendMail,
  sendReplyMail
}
