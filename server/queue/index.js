import kue from 'kue'
import Ticket from '../models/Ticket'
import Notification from '../models/Notification'
const socketio = require('socket.io-emitter')({ host: '127.0.0.1', port: 6379 })

const jobs = kue.createQueue({
  prefix: 'q',
  redis: {
    port: 6379,
    host: 'localhost'
  }
})

module.exports = {
  jobs,
  queueEvents: () => {
    jobs.process('snooze', (job, done) => {
      Ticket.findOne({ _id: job.data }).exec((err, ticket) => {
        if (err) throw new Error(err)

        if (ticket && ticket.user) {
          const n = {
            user: [ticket.user],
            type: 'deadline',
            ticket: ticket._id,
            text: ['До окончания дедлайна для тикета', 'осталось', '30', 'минут']
          }
          const notification = new Notification(n)
          notification.save((err, n) => {
            if (err) throw new Error(err)

            n.populate('user ticket', (err) => {
              if (err) throw new Error(err)

              socketio.emit('notificationAdded', n)
            })
          })
        }
      })
      done(null, job.data)
    })
  }
}
