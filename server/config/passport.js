const passport = require('passport')
const LocalStrategy = require('passport-local').Strategy
import User from '../models/user'

passport.use(new LocalStrategy({
  usernameField: 'email',
  passReqToCallback: true
}, (req, username, password, done) => {
  User.findOne({ $or: [
    { email: username },
    { username: req.body.email }
  ] }, (err, user) => {
    if (err) return done(err)
    if (!user) {
      return done(null, false, { message: 'Пользователь не найден', origin: 'email' })
    }

    if (!user.validPassword(password)) {
      return done(null, false, { message: 'Неправильное имя пользователя или пароль', origin: 'password' })
    }

    return done(null, user)
  })
}))
