import HistoryStaff from '../models/HistoryStaff'
import HistoryClient from '../models/HistoryClient'
import HistoryProfile from '../models/HistoryProfile'
import Notification from '../models/Notification'
import Ticket from '../models/Ticket'

module.exports = function (io) {
  io.sockets.on('connection', function (socket) {
    socket.on('addHistoryStaff', (data) => {
      var myHistory = data
      if (myHistory.user.length > 1 && myHistory.user[1] === 'user1') {
        myHistory.user[1] = null
      }
      if (myHistory.ticket) {
        myHistory.ticket.forEach((item) => {
          if (item !== 'all') {
            var history = new HistoryStaff()
            history.user = myHistory.user
            history.type = myHistory.type
            history.ticket = item
            history.text = myHistory.text
            history.save((err, h) => {
              if (err) throw new Error(err)

              h.populate('user ticket', (err) => {
                if (err) throw new Error(err)

                socket.emit('historyStaffAdded', h)
              })
            })
          }
        })
      }
    })

    socket.on('addHistoryClient', (data) => {
      var myHistory = data
      myHistory.ticket.forEach((item) => {
        if (item !== 'all') {
          var history = new HistoryClient()
          history.user = myHistory.user[0]
          history.ticket = item
          history.text = myHistory.text
          history.save((err, h) => {
            if (err) throw new Error(err)

            h.populate('user ticket', (err) => {
              if (err) throw new Error(err)

              socket.emit('historyClientAdded', h)
            })
          })
        }
      })
    })

    socket.on('addNotification', (data) => {
      var myNotification = data
      myNotification.ticket.forEach((item) => {
        if (item !== 'all') {
          var notification = new Notification()
          notification.user = myNotification.user
          notification.ticket = item
          notification.text = myNotification.text
          notification.type = myNotification.type
          notification.save((err, n) => {
            if (err) throw new Error(err)

            n.populate('user ticket', (err) => {
              if (err) throw new Error(err)

              socket.emit('notificationAdded', n)
            })
          })
        }
      })
    })

    socket.on('addNotificationProfile', (data) => {
      var notification = new Notification(data)
      notification.save((err, n) => {
        if (err) throw new Error(err)

        n.populate('user ticket', (err) => {
          if (err) throw new Error(err)

          socket.emit('notificationAdded', n)
        })
      })
    })

    socket.on('addNotificationAssign', (data) => {
      var ticketIds = data.tickets
      ticketIds.forEach((item, index) => {
        if (item === 'all') {
          ticketIds.splice(index, 1)
        }
      })
      Ticket.find({ _id: ticketIds }, (err, tickets) => {
        if (err) throw new Error(err)

        var tempNotification = {
          user: [null, data.currentUser],
          type: 'ticket',
          ticket: null,
          text: data.text
        }
        tickets.forEach((item) => {
          if (item.user !== null && item.user.toString() !== data.currentUser.toString()) {
            tempNotification.user[0] = item.user
            tempNotification.ticket = item._id
            const notification = new Notification(tempNotification)
            notification.save((err, n) => {
              if (err) throw new Error(err)

              n.populate('user ticket', (err) => {
                if (err) throw new Error(err)

                socket.emit('notificationAdded', n)
              })
            })
          }
          if (data.both && item.client.toString() !== data.currentUser.toString()) {
            tempNotification.user[0] = item.client
            tempNotification.ticket = item._id
            const notification = new Notification(tempNotification)
            notification.save((err, n) => {
              if (err) throw new Error(err)

              n.populate('user ticket', (err) => {
                if (err) throw new Error(err)

                socket.emit('notificationAdded', n)
              })
            })
          }
        })
      })
    })

    socket.on('addProfileHistory', (data) => {
      data.forEach((item) => {
        const history = new HistoryProfile(item)
        history.save((err, h) => {
          if (err) throw new Error(err)

          h.populate('user userTo', (err) => {
            if (err) throw new Error(err)

            socket.emit('profileHistoryAdded', h)
          })
        })
      })
    })
  })
}
