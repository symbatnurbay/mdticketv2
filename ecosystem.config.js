module.exports = {
  apps: [
    {
      name: 'tiket',
      script: './index.js',
      env: {
        'NODE_ENV': 'production'
      }
    }
  ]
}
