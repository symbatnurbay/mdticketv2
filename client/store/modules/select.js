const state = {
  send: [
    {
      name: 'Отправить открытым',
      _id: 'open'
    },
    {
      name: 'Отправить в ожидании',
      _id: 'pending'
    },
    {
      name: 'Отправить закрытым',
      _id: 'closed'
    }
  ],
  assign: [
    {
      name: 'Открытый',
      _id: 'open'
    },
    {
      name: 'В ожидании',
      _id: 'pending'
    },
    {
      name: 'Закрытый',
      _id: 'closed'
    }
  ],
  snooze: [
    {
      name: 'на 4 часа',
      _id: '1'
    },
    {
      name: 'на 1 день',
      _id: '2'
    },
    {
      name: 'на 2 дня',
      _id: '3'
    },
    {
      name: 'на 4 дней',
      _id: '4'
    },
    {
      name: 'на неделю',
      _id: '5'
    },
    {
      name: 'на 2 недели',
      _id: '6'
    },
    {
      name: 'на 1 месяц',
      _id: '7'
    }
  ]
}

const getters = {
  getSendTypes: state => state.send,
  getAssignTypes: state => state.assign,
  getSnoozeTypes: state => state.snooze
}

export default {
  state,
  getters
}
