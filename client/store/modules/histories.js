const state = {
  actions: [],
  actionsClient: [],
  myProfileHistory: []
}

const getters = {
  getAllActions: state => (type) => {
    return type === 'client' ? state.actionsClient : type === 'staff' ? state.actions : state.myProfileHistory
  }
}

const mutations = {
  fetchHistoryStaff (state, payload) {
    state.actions = payload.history
  },
  fetchHistoryClient (state, payload) {
    state.actionsClient = payload.history
  },
  fetchHistoryProfile (state, payload) {
    state.myProfileHistory = payload.history
  },
  addHistoryStaff (state, payload) {
    state.actions.unshift(payload)
  },
  addHistoryClient (state, payload) {
    state.actionsClient.unshift(payload)
  },
  addHistoryProfile (state, payload) {
    state.myProfileHistory.unshift(payload)
  },
  changeUserOfHistoryStaff (state, payload) {
    state.actions.forEach((item, index) => {
      item.user.forEach((item2, i) => {
        if (item2._id.toString() === payload.user._id.toString()) {
          state.actions[index].user[i] = payload.user
        }
      })
    })
  },
  changeUserOfHistoryClient (state, payload) {
    state.actionsClient.forEach((item, index) => {
      if (item.user._id.toString() === payload.user._id.toString()) {
        state.actionsClient[index].user = payload.user
      }
    })
  }
}

export default {
  state,
  getters,
  mutations
}
