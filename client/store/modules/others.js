const state = {
  currentPage: 'open'
}

const getters = {
  getCurrentPage: state => state.currentPage
}

const mutations = {
  changePage (state, payload) {
    state.currentPage = payload.currentPage
  }
}

const actions = {
  changeCurrentPage ({ commit, state, getters }, payload) {
    if (state.currentPage === 'settings' || state.currentPage === 'add') {
      if (payload.currentPage === 'open' || payload.currentPage === 'closed' || payload.currentPage === 'pending') {
        commit('changeNav', { currentNav: 'mine' })
      }
    }
    commit('changePage', payload)
    if (getters.getChecked.length > 0) {
      commit('emptyChecked')
    }
  },
  changeStates ({ commit, getters }, payload) {
    if (getters.getCurrentUser._id.toString() === payload.user._id.toString()) {
      commit('currentUser', payload)
    }
    if (payload.user.type === 'client') {
      commit('changeOneClient', payload)
    } else {
      commit('changeOneStaff', payload)
    }
    commit('changeUserOfTicket', payload)
    commit('changeUserOfHistoryStaff', payload)
    commit('changeUserOfHistoryClient', payload)
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}
