const state = {
  usersOfCompany: [
    {
      _id: 'user1',
      username: 'Всем'
    }
  ],
  clients: [],
  checkedUsers: [],
  currentUser: {}
}

const getters = {
  getUsersOfCompany: state => state.usersOfCompany,
  getCheckedUsers: state => state.checkedUsers,
  getClients: state => state.clients,
  getStaff: state => (id) => {
    return state.usersOfCompany.filter(user => {
      if (user._id.toString() !== id && user._id.toString() !== 'user1') {
        return user
      }
    })
  },
  getCurrentUser: state => state.currentUser
}

const mutations = {
  fetchUsers (state, payload) {
    state.usersOfCompany.push(...payload)
  },
  fetchClients (state, payload) {
    state.clients = payload
  },
  emptyCheckedUsers (state) {
    state.checkedUsers = []
  },
  checkedAllUsers (state, payload) {
    state.checkedUsers = payload.arr
  },
  checkedOneUserRemove (state, payload) {
    state.checkedUsers.forEach((item, index) => {
      if ((item === payload.id) || (item === 'all')) {
        state.checkedUsers.splice(index, 1)
      }
    })
  },
  checkedOneUserAdd (state, payload) {
    state.checkedUsers.push(payload.id)
  },
  deleteStaff (state, payload) {
    state.usersOfCompany.forEach((item, index) => {
      if (item._id === payload.id) {
        state.usersOfCompany.splice(index, 1)
      }
    })
  },
  deleteClient (state, payload) {
    state.clients.forEach((item, index) => {
      if (item._id === payload.id) {
        state.clients.splice(index, 1)
      }
    })
  },
  addStaff (state, payload) {
    state.usersOfCompany.push(payload)
  },
  addClient (state, payload) {
    state.clients.push(payload)
  },
  currentUser (state, payload) {
    state.currentUser = payload.user
    state.currentUser.emailModule = payload.user.company.emailModule
    state.currentUser.company = payload.user.company.name
  },
  changeOneClient (state, payload) {
    state.clients.forEach((item, index) => {
      if (item._id.toString() === payload.user._id.toString()) {
        state.clients[index] = payload.user
      }
    })
  },
  changeOneStaff (state, payload) {
    state.usersOfCompany.forEach((item, index) => {
      if (item._id.toString() === payload.user._id.toString()) {
        state.usersOfCompany[index] = payload.user
      }
    })
  }
}

const actions = {
  changeCheckedUsers ({ commit, state, getters }, payload) {
    if (payload.type === 'all') {
      if (state.checkedUsers.indexOf('all') > -1) {
        commit('emptyCheckedUsers')
      } else {
        var temp = ['all']
        if (payload.user === 'staff') {
          getters.getStaff.forEach(item => {
            temp.push(item._id)
          })
        } else {
          getters.getClients.forEach(item => {
            temp.push(item._id)
          })
        }
        commit('checkedAllUsers', { arr: temp })
      }
    } else {
      if (state.checkedUsers.indexOf(payload.value) > -1) {
        commit('checkedOneUserRemove', { id: payload.value })
      } else {
        commit('checkedOneUserAdd', { id: payload.value })
      }
    }
  },
  deleteUsers ({ commit }, payload) {
    var type = payload.type === 'staff' ? 'deleteStaff' : 'deleteClient'
    payload.users.forEach((user) => {
      commit(type, { id: user })
    })
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}
