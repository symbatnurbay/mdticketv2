import Auth from '../../utils/auth.js'

const state = {
  tickets: [],
  currentNav: 'mine',
  checked: []
}

const getters = {
  getAllTickets: state => state.tickets,
  getTicketsByType: (state, getters) => () => {
    return state.tickets.filter(ticket => {
      if (state.currentNav === 'mine') {
        return ticket.type === getters.getCurrentPage && ticket.user && ticket.user._id === Auth().currentUser().id
      } else if (state.currentNav === 'anyone') {
        return ticket.type === getters.getCurrentPage && (!ticket.user || ticket.user === undefined || ticket.user === null || ticket.user === '')
      } else {
        return ticket.type === getters.getCurrentPage
      }
    })
  },
  getTicketsByTypeClient: (state, getters) => () => {
    return state.tickets.filter(ticket => {
      return ticket.type === getters.getCurrentPage
    })
  },
  getCurrentNav: state => state.currentNav,
  getOneTicketById: state => (id) => {
    return state.tickets.filter((ticket) => {
      return ticket._id.toString() === id
    })[0]
  },
  getChecked: state => state.checked,
  getTicketsOfClient: state => (id) => {
    return state.tickets.filter(ticket => {
      return ticket.client._id === id
    })
  }
}

const mutations = {
  changeNav (state, payload) {
    state.currentNav = payload.currentNav
  },
  addNewTicket (state, payload) {
    state.tickets.unshift(payload)
  },
  fetchTickets (state, payload) {
    state.tickets = payload.tickets
  },
  changeTicket (state, payload) {
    state.tickets.forEach((item, index) => {
      if (item._id === payload.ticket._id) {
        delete state.tickets[index]
        state.tickets.unshift(payload.ticket)
      }
    })
  },
  emptyChecked (state) {
    state.checked = []
  },
  checkedAll (state, payload) {
    state.checked = payload.arr
  },
  checkedOneRemove (state, payload) {
    state.checked.forEach((item, index) => {
      if ((item === payload.id) || (item === 'all')) {
        state.checked.splice(index, 1)
      }
    })
  },
  checkedOneAdd (state, payload) {
    state.checked.push(payload.id)
  },
  changeFavOne (state, payload) {
    state.tickets.forEach((item, index) => {
      if (item._id === payload.id) {
        if (payload.type === 'one') {
          state.tickets[index].favorite = !state.tickets[index].favorite
        } else {
          state.tickets[index].favorite = !payload.value
        }
      }
    })
  },
  deleteOneTicket (state, payload) {
    state.tickets.forEach((item, index) => {
      if (item._id === payload.id) {
        state.tickets.splice(index, 1)
      }
    })
  },
  deleteTickets (state) {
    state.checked.forEach(item => {
      if (item !== 'all') {
        state.tickets.forEach((itemData, index) => {
          if (itemData._id === item) {
            state.tickets.splice(index, 1)
          }
        })
      }
    })
  },
  assignUser (state, payload) {
    state.tickets.forEach((ticket, index) => {
      if (ticket._id === payload.ticket) {
        console.log('here')
        state.tickets[index].user = payload.user
      }
    })
  },
  changeType (state, payload) {
    state.tickets.forEach((ticket, index) => {
      if (ticket._id === payload.ticket) {
        state.tickets[index].type = payload.type
      }
    })
  },
  snoozeTicket (state, payload) {
    state.tickets.forEach((ticket, index) => {
      if (ticket._id === payload.ticket) {
        state.tickets[index].deadline = payload.date
      }
    })
  },
  changeUserOfTicket (state, payload) {
    state.tickets.forEach((ticket, index) => {
      if (payload.user.type === 'client' && ticket.client._id.toString() === payload.user._id.toString()) {
        state.tickets[index].client = payload.user
      } else if (payload.user.type !== 'client' && ticket.user && ticket.user._id.toString() === payload.user._id.toString()) {
        state.tickets[index].user = payload.user
      }
    })
  },
  changeRead (state, payload) {
    state.tickets.forEach((ticket, index) => {
      if (ticket._id.toString() === payload) {
        state.tickets[index].read = true
      }
    })
  }
}

const actions = {
  changeNav ({ commit, getters }, payload) {
    commit('changeNav', payload)
    if (getters.getChecked.length > 0) {
      commit('emptyChecked')
    }
    if (getters.getCheckedUsers.length > 0) {
      commit('emptyCheckedUsers')
    }
  },
  changeChecked ({ commit, state, getters }, payload) {
    if (payload.type === 'all') {
      if (state.checked.indexOf('all') > -1) {
        commit('emptyChecked')
      } else {
        var t = Auth().currentUser().type === 'client' ? getters.getTicketsByTypeClient() : getters.getTicketsByType()
        var temp = ['all']
        t.forEach(item => {
          temp.push(item._id)
        })
        commit('checkedAll', { arr: temp })
      }
    } else {
      if (state.checked.indexOf(payload.value) > -1) {
        commit('checkedOneRemove', { id: payload.value })
      } else {
        commit('checkedOneAdd', { id: payload.value })
      }
    }
  },
  changeFavAll ({ commit, getters }, payload) {
    getters.getTicketsByType().forEach((item) => {
      commit('changeFavOne', { id: item._id, value: payload.value, type: 'all' })
    })
  },
  addNewTicket ({ commit }, payload) {
    commit('changePage', { currentPage: 'open' })
    commit('addNewTicket', payload)
    commit('changeNav', { currentNav: 'anyone' })
  },
  assignUserChecked ({ commit, getters }, payload) {
    getters.getChecked.forEach(item => {
      if (item !== 'all') {
        commit('assignUser', { ticket: item, user: payload.user })
      }
    })
  },
  changeTypeChecked ({ commit, getters }, payload) {
    getters.getChecked.forEach(item => {
      if (item !== 'all') {
        commit('changeType', { ticket: item, type: payload.type })
      }
    })
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}
