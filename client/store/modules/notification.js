const state = {
  notificationList: []
}

const getters = {
  getNotifications: state => state.notificationList,
  getUnread: state => () => {
    return state.notificationList.filter((item) => {
      if (!item.read) {
        return item
      }
    })
  }
}

const mutations = {
  fetchNotifications (state, payload) {
    state.notificationList = payload
  },
  addNewNotification (state, payload) {
    state.notificationList.unshift(payload)
  },
  changeUnread (state, payload) {
    state.notificationList.forEach((item, index) => {
      payload.ids.forEach((item2) => {
        if (item._id === item2) {
          state.notificationList[index].read = true
        }
      })
    })
  }
}

export default {
  state,
  getters,
  mutations
}
