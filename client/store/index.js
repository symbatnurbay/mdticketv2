import Vue from 'vue'
import Vuex from 'vuex'
import others from './modules/others'
import tickets from './modules/tickets'
import histories from './modules/histories'
import user from './modules/user'
import select from './modules/select'
import notification from './modules/notification'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    others,
    tickets,
    histories,
    user,
    select,
    notification
  }
})
