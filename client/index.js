import Vue from 'vue'
import VueRouter from 'vue-router'
import VueSocketio from 'vue-socket.io'

import App from './App.vue'

import Timeline from 'Atoms'
import router from './router'

import Api from './utils/api'
import Auth from './utils/auth'
import store from './store'
import VModal from 'vue-js-modal'
import Notifications from 'vue-notification'
import VueScrollTo from 'vue-scrollto'
import VueAutosize from 'vue-autosize'
Vue.use(VueAutosize)

Vue.use(Notifications)
Vue.use(VModal)
Vue.use(VueScrollTo)

Vue.use(VueRouter)
Vue.use(Timeline)
// Vue.use(VueSocketio, 'http://localhost:8000/')
Vue.use(VueSocketio, 'http://146.185.131.163:8000/')

const apiInstance = new Api()
Vue.prototype.$api = apiInstance
Vue.prototype.$auth = new Auth()

const app = new Vue({
  router,
  store,
  ...App
})

app.$mount('#app')
