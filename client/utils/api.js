import axios from 'axios'
import Auth from './auth'
axios.defaults.headers.common['Authorization'] = `Bearer ${Auth().getToken()}`

export default function (option) {
  return {
    addUser: (user, type) => {
      return new Promise((resolve, reject) => {
        axios.post(`/api/${type}`, user)
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    },
    getClients: () => {
      return new Promise((resolve, reject) => {
        axios.get('/api/clients')
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    },
    changeAvatar: (id, file) => {
      return new Promise((resolve, reject) => {
        axios.post(`/api/users/${id}/avatar`, file)
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    },
    deleteImage: (id) => {
      return new Promise((resolve, reject) => {
        axios.delete(`/api/users/${id}/avatar`)
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    },
    getUsers: (companyId) => {
      return new Promise((resolve, reject) => {
        axios.get('/api/users')
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    },
    getUser: id => {
      return new Promise((resolve, reject) => {
        axios.get(`/api/users/${id}`)
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    },
    updateUser: (id, data) => {
      return new Promise((resolve, reject) => {
        axios.put(`/api/users/${id}`, data)
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    },
    getMe: () => {
      return new Promise((resolve, reject) => {
        axios.get('/api/users/me')
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    },
    updateMe: (data) => {
      return new Promise((resolve, reject) => {
        axios.put('/api/users/me', data)
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    },
    addTicket: (ticket) => {
      return new Promise((resolve, reject) => {
        axios.post('/api/tickets', ticket)
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    },
    getTickets: () => {
      return new Promise((resolve, reject) => {
        axios.get('/api/tickets/gettickets')
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    },
    addAnswer: (ticket, ticketId) => {
      return new Promise((resolve, reject) => {
        axios.post('/api/tickets/addanswer/' + ticketId, ticket)
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    },
    changeFavAll: (tickets, value) => {
      return new Promise((resolve, reject) => {
        axios.post('/api/tickets/changefavall', { tickets, value })
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    },
    changeFavOne: (id, value) => {
      return new Promise((resolve, reject) => {
        axios.post('/api/tickets/changefavone', { id, value })
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    },
    deleteOneTicket: (id) => {
      return new Promise((resolve, reject) => {
        axios.post('/api/tickets/deleteticket/' + id)
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    },
    deleteTickets: (tickets) => {
      return new Promise((resolve, reject) => {
        axios.post('/api/tickets/deletetickets', { tickets })
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    },
    assignUserChecked: (user, tickets) => {
      return new Promise((resolve, reject) => {
        axios.post('/api/tickets/assignusers', { user, tickets })
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    },
    assignUser: (user, ticket) => {
      return new Promise((resolve, reject) => {
        axios.post('/api/tickets/assignuser/' + ticket, { user })
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    },
    changeTypeChecked: (type, tickets) => {
      return new Promise((resolve, reject) => {
        axios.post('/api/tickets/changetypes', { type, tickets })
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    },
    changeType: (ticket, type) => {
      return new Promise((resolve, reject) => {
        axios.post('/api/tickets/changetype/' + ticket, { type })
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    },
    snoozeTicket: (ticket, value) => {
      return new Promise((resolve, reject) => {
        axios.post('/api/tickets/snoozeticket/' + ticket, { value })
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    },
    getHistory: () => {
      return new Promise((resolve, reject) => {
        axios.get('/api/history/gethistory')
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    },
    deleteUser: (users, type) => {
      return new Promise((resolve, reject) => {
        axios.post('/api/users/delete', { users, type })
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    },
    getNotifications: () => {
      return new Promise((resolve, reject) => {
        axios.get('/api/notification')
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    },
    changeUnread: (notifications) => {
      return new Promise((resolve, reject) => {
        axios.post('/api/notification/unread', notifications)
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    },
    getHistoryProfile: (id) => {
      return new Promise((resolve, reject) => {
        axios.get('/api/history/profile/' + id)
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    },
    getEmails: (data) => {
      return new Promise((resolve, reject) => {
        axios.post('/api/email/', data)
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    },
    changeRead: (id) => {
      return new Promise((resolve, reject) => {
        axios.post('/api/tickets/changeread/' + id)
          .then((res) => {
            resolve(res)
          })
          .catch((err) => {
            reject(err)
          })
      })
    }
  }
}
