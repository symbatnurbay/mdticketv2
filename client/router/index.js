import VueRouter from 'vue-router'

import Registration from 'Pages/registration'
import Login from 'Pages/login'
import Auth from '../utils/auth'
import TicketsList from 'Pages/tickets'
import TicketsListClient from 'Pages/ticketsClient'
import OneTicket from 'Pages/oneTicket'
import OneTicketClient from 'Pages/oneTicketClient'
import Settings from 'Pages/settings'
import SettingsClient from 'Pages/settingsClient'
import Staff from 'Pages/settings/staff'
import Clients from 'Pages/settings/clients'
import OneClient from 'Pages/settings/one'
import Personal from 'Pages/settings/personal'
import Check from 'Pages/checkEmail'

import store from '../store'

const routes = [
  {
    path: '/',
    component: Auth().isLoggedIn() && Auth().currentUser().type === 'client' ? TicketsListClient : TicketsList,
    name: 'Main'
  },
  {
    path: '/signup',
    component: Registration,
    name: 'Signup'
  },
  {
    path: '/login',
    component: Login
  },
  {
    path: '/oneticket',
    component: OneTicket
  },
  {
    path: '/ticket',
    component: OneTicketClient
  },
  {
    path: '/check',
    component: Check
  },
  {
    path: '/settings',
    component: Auth().isLoggedIn() && Auth().currentUser().type === 'admin' ? Settings : SettingsClient,
    name: 'settings',
    children: [
      {
        path: 'staff',
        component: Staff,
        beforeEnter: (to, from, next) => {
          store.commit('changeNav', { currentNav: 'staff' })
          next()
        }
      },
      {
        path: 'clients',
        component: Clients,
        beforeEnter: (to, from, next) => {
          store.commit('changeNav', { currentNav: 'clients' })
          next()
        }
      },
      {
        path: 'personal',
        component: Personal,
        name: 'personal',
        beforeEnter: (to, from, next) => {
          store.commit('changeNav', { currentNav: 'personal' })
          next()
        }
      },
      {
        path: ':id',
        component: OneClient
      }
    ]
  }
]

const router = new VueRouter({
  // mode: 'history',
  history: true,
  routes
})

router.beforeEach((to, from, next) => {
  if (Auth().isLoggedIn()) {
    if (to.path === '/login' || to.path === '/signup') {
      store.commit('changePage', { currentPage: 'open' })
      return next('/')
    } else if (to.path === '/settings/clients' || to.path === '/settings/staff' || to.path === '/settings/personal') {
      if (Auth().currentUser().type === 'admin') {
        store.commit('changePage', { currentPage: 'settings' })
        next()
      } else {
        return next('/')
      }
    } else if (to.path === '/settings') {
      if (Auth().currentUser().type !== 'admin') {
        store.commit('changePage', { currentPage: 'settings' })
        next()
      } else {
        return next('/settings/personal')
      }
    }
    next()
  } else {
    if (to.path !== '/signup' && to.path !== '/login') {
      next({
        path: '/login'
      })
    } else {
      next()
    }
  }
})

export default router
