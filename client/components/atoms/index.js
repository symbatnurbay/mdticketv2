import * as atoms from './register.js'

const Components = {}

Components.install = (Vue) => {
  for (let atom in atoms) {
    const componentInstaller = atoms[atom]

    Vue.use(componentInstaller)
  }
}

export default Components
